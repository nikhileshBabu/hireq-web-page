<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Uviqo</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">

    <!-- Favicon -->
    <link href="img/favicon.ico" rel="icon">

    <!-- Google Web Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Heebo:wght@400;500&family=Roboto:wght@400;500;700&display=swap" rel="stylesheet"> 

    <!-- Icon Font Stylesheet -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">

    <!-- Libraries Stylesheet -->
    <link href="lib/animate/animate.min.css" rel="stylesheet">
    <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="lib/lightbox/css/lightbox.min.css" rel="stylesheet">

    <!-- Customized Bootstrap Stylesheet -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Template Stylesheet -->
    <link href="css/style.css" rel="stylesheet">
</head>

<body>
    <div class="container-xxl bg-white p-0">
        <!-- Spinner Start -->
        <div id="spinner" class="show bg-white position-fixed translate-middle w-100 vh-100 top-50 start-50 d-flex align-items-center justify-content-center">
            <div class="spinner-grow text-primary" style="width: 3rem; height: 3rem;" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>
        <!-- Spinner End -->


        <!-- Navbar & Hero Start -->
        <div class="container-xxl position-relative p-0">
            <nav class="navbar navbar-expand-lg navbar-light px-4 px-lg-5 py-2">
                <a href="" class="navbar-brand p-0">
                   <img src="img/logo.png" alt="Logo">
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse">
                    <span class="fa fa-bars"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarCollapse">
                    <div class="navbar-nav ms-auto py-0">
                        <a href="index.html" class="nav-item nav-link active">For Candidates</a>
                        <div class="nav-item dropdown">
                            <a href="recruiters.html" class="nav-link dropdown-toggle" >For Recruiters</a>
                            <div class="dropdown-menu m-0">
                                <a href="features.html" class="dropdown-item">Features</a>
                                <a href="pricing.html" class="dropdown-item">Pricing</a>

                            </div>
                        </div>
                        <a href="contact.html" class="nav-item nav-link">Contact</a>
                    </div>
                    
                   <a href=""class=" btnn rounded-pills py-2 px-4 ms-3">LOGIN</a>
                    <a href="" class="btn btn-secondary text-light rounded-pills py-2 px-4 ms-3">SIGN IN</a>
                </div>
            </nav>

            <div class="container-xxl    mb-5">
                <div class="container my-5 py-5 px-lg-5">
                    <div class="row g-5">
                        <div class="col-lg-8 text-center text-lg-start">
                            <h1 class=" font_header mb-4 animated zoomIn">Find the perfect job<br>that you deserve</h1>
                            
                            <form >
                              <div class="input-icons">
                                <i class="fa fa-search icon" aria-hidden="true"></i>
                                <input placeholder="Job title" class="btn btn-light py-sm-3 rounded-pills px-sm-5 input-field " type="text">
                                <!-- <i class="fa fa-map-marker icon" aria-hidden="true"></i> -->
                                <select name="pets" id="pet-select" class="input-field btn btn-light py-sm-3 rounded-pills px-sm-2" style="color: #7a7474;">
    <option value="">India</option>
    <option value="">Thiruvananthapuram</option>
    <option value="">Cochin</option>
    <option value="">Chennai</option>
    <option value="">Ernakulam</option>
    
</select>
                                <button class="btn_m btn btn-secondary text-light rounded-pills mt-2 py-2 px-5 ms-3"> Search </button>
                              </div>
                             </form>

                            <p style="color: #9f9b99!important;" class=" mt-3 animated zoomIn">Search  keywords e.g. product designer</p>
                        </div>
                        <div class="col-lg-4 text-center text-lg-start">
                            <img class="img-fluid" src="img/banner.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Navbar & Hero End -->
<div class="container-xxl py-5" style="background-color:#F0F8FEFF;">
            <div class="container px-lg-5">
                <div class="section-title position-relative text-left mb-5 pb-2 wow fadeInUp" data-wow-delay="0.1s">
                     <h2 class="mt-2">Get hired in top companies</h2>
                </div>
                
                <div class="row g-4 portfolio-container">
                    <div class="col-lg-2 col-md-3 portfolio-item first wow zoomIn" data-wow-delay="0.1s">
                        <div class="position-relative  overflow-hidden">
                            <img class="img-fluid w-100" src="img/job_logo.png" alt="">
                            
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-3 portfolio-item second wow zoomIn" data-wow-delay="0.3s">
                        <div class="position-relative  overflow-hidden">
                            <img class="img-fluid w-100" src="img/job_logo.png" alt="">
                            
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-3 portfolio-item first wow zoomIn" data-wow-delay="0.6s">
                        <div class="position-relative  overflow-hidden">
                            <img class="img-fluid w-100" src="img/job_logo.png" alt="">
                           
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-3 portfolio-item second wow zoomIn" data-wow-delay="0.1s">
                        <div class="position-relative  overflow-hidden">
                            <img class="img-fluid w-100" src="img/job_logo.png" alt="">
                            
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-3 portfolio-item first wow zoomIn" data-wow-delay="0.3s">
                        <div class="position-relative  overflow-hidden">
                            <img class="img-fluid w-100" src="img/job_logo.png" alt="">
                            
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-3 portfolio-item second wow zoomIn" data-wow-delay="0.6s">
                        <div class="position-relative  overflow-hidden">
                            <img class="img-fluid w-100" src="img/job_logo.png" alt="">
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="container-xxl py-5">
            <div class="container px-lg-5">
                <div class="section-title position-relative text-left mb-5 pb-2 wow fadeInUp"  data-wow-delay="0.1s">
                    
                    <h2 class="mt-2">Categories</h2>
                </div>
                <div class="row g-4">
                    <div class="col-lg-4 col-md-6 wow zoomIn" data-wow-delay="0.1s">
                        <div style="height: unset!important;background-color: #F0F8FEFF;" class="service-itemm service-item d-flex flex-column justify-content-center  rounded">
                            <div class=" flex-shrink-0">
                                <img src="img/categories/business.png">
                            </div>
                            <h5 class="mb-3 mt-5">Business development</h5>
                            <p>415 Vacancy</p>
                            
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 wow zoomIn" data-wow-delay="0.3s">
                       <div style="height: unset!important;background-color: #F0F8FEFF;" class="service-itemm service-item d-flex flex-column justify-content-center  rounded">
                            <div class="flex-shrink-0">
                                <img src="img/categories/customer.png">
                            </div>
                            <h5 class="mb-3 mt-5">Customer Service</h5>
                            <p>235 Vacancy</p>
                           
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 wow zoomIn" data-wow-delay="0.6s">
                        <div style="height: unset!important;background-color: #F0F8FEFF;" class="service-itemm service-item d-flex flex-column justify-content-center  rounded">
                            <div class=" flex-shrink-0">
                               <img src="img/categories/development.png">
                            </div>
                            <h5 class="mb-3 mt-5">Development</h5>
                            <p>624 Vacancy</p>
                            
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 wow zoomIn" data-wow-delay="0.1s">
                        <div style="height: unset!important;background-color: #F0F8FEFF;" class="service-itemm service-item d-flex flex-column justify-content-center  rounded">
                            <div class=" flex-shrink-0">
                                <img src="img/categories/market.png">
                            </div>
                            <h5 class="mb-3 mt-5">Marketing & Management</h5>
                            <p>268 Vacancy</p>
                            
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 wow zoomIn" data-wow-delay="0.3s">
                       <div style="height: unset!important;background-color: #F0F8FEFF;" class="service-itemm service-item d-flex flex-column justify-content-center  rounded">
                            <div class=" flex-shrink-0">
                                <img src="img/categories/sale.png">
                            </div>
                            <h5 class="mb-3 mt-5">Sales & Communication</h5>
                            <p>156 Vacancy</p>
                           
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 wow zoomIn" data-wow-delay="0.6s">
                        <div style="height: unset!important;background-color: #F0F8FEFF;" class="service-itemm service-item d-flex flex-column justify-content-center  rounded">
                            <div class=" flex-shrink-0">
                                <img src="img/categories/project.png">
                            </div>
                            <h5 class="mb-3 mt-5">Project Management</h5>
                            <p>162 Vacancy</p>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!-- About Start -->
        <div class="container-xxl py-5">
            <div class="container px-lg-5">
                <div class="row g-5">

                    <div class="col-lg-6">
                        <img class="img-fluid wow zoomIn" data-wow-delay="0.5s" src="img/job_banner.png">
                    </div>

                    <div class="col-lg-6 wow fadeInUp" data-wow-delay="0.1s">
                        <div class="section-title position-relative mb-4 pb-2">
                            
                            <h2 class="mt-2">Help you to get the best job that fits you</h2>
                        </div>
                        <p class="mb-4">Leverage agile frameworks to provide a robust synopsis for high level overviews.Iterative approach</p>
                        <div class="row g-3">
                            <div class="col-sm-12">
                                <h6 class="mb-3"><i class="fa fa-check text-primary me-2"></i>Bring to the table win-win survival</h6>
                                <h6 class="mb-3"><i class="fa fa-check text-primary me-2"></i>Capitalize on low hanging fruit to identify</h6>
                                <h6 class="mb-3"><i class="fa fa-check text-primary me-2"></i>But I must explain to you how all this</h6>
                            </div>
                        </div>
                        
                    </div>
                    
                </div>
            </div>
        </div>
        <!-- About End -->

        <div class="container-xxl py-5" style="background-color:#F0F8FEFF;">
            <div class="container px-lg-5" >
                <div style="display:flex;" class="section-title position-relative text-left mb-5 pb-2 wow fadeInUp" data-wow-delay="0.1s">
                   <div class="col-md-6">
                    <h2 class="mt-2">Featured Jobs</h2></div>
                    <div class="col-md-6 text-md-end">
                    <button class="btnn rounded-pills py-2 px-3 ms-3"> EXPLORE ALL </button></div>
                </div>
                <div class="row g-4">
                    <div class="col-lg-4 col-md-6 wow zoomIn" data-wow-delay="0.1s">
                        <div class="service-item d-flex flex-column justify-content-center  rounded">
                            <div class=" flex-shrink-0">
                                <img src="img/jobs_logo.png">

                            </div>
                            <p class="mt-2">Uviqo</p>
                            <h5 class="mb-3">Java Developer</h5>
                            <p><span style="margin-left: unset!important;" class="ser">Cochin</span><span class="ser">80k to 90k</span></p>
                            <p>We are looking for Enrolment Advisors who are looking to take 30-35 appointments per week All leads are pre-scheduled.</p>
                    <div class="col-md-12  " style="display:flex!important; ">
                          <a href="" class=" b_btn btn btn-secondary text-light rounded-pills ms-3">Apply Now</a>  
                          <a href=""class=" b_btn btnn rounded-pills  ms-3">Save Later</a>
                    </div>
                </div>
                    </div>
                    <div class="col-lg-4 col-md-6 wow zoomIn" data-wow-delay="0.3s">
                        <div class="service-item d-flex flex-column justify-content-center  rounded">
                            <div class=" flex-shrink-0">
                                <img src="img/jobs_logo.png">

                            </div>
                            <p>Uviqo</p>
                            <h5 class="mb-3">Java Developer</h5>
                            <p><span style="margin-left: unset!important;" class="ser">Cochin</span><span class="ser">80k to 90k</span></p>
                            <p>We are looking for Enrolment Advisors who are looking to take 30-35 appointments per week All leads are pre-scheduled.</p>
                             <div class="col-md-12  " style="display:flex!important; ">
                          <a href="" class=" b_btn btn btn-secondary text-light rounded-pills ms-3">Apply Now</a>  
                          <a href=""class=" b_btn btnn rounded-pills  ms-3">Save Later</a>
                    </div>
                </div>
                    </div>
                    <div class="col-lg-4 col-md-6 wow zoomIn" data-wow-delay="0.6s">
                        <div class="service-item d-flex flex-column justify-content-center  rounded">
                            <div class=" flex-shrink-0">
                                <img src="img/jobs_logo.png">

                            </div>
                            <p>Uviqo</p>
                            <h5 class="mb-3">Java Developer</h5>
                            <p><span style="margin-left: unset!important;" class="ser">Cochin</span><span class="ser">80k to 90k</span></p>
                            <p>We are looking for Enrolment Advisors who are looking to take 30-35 appointments per week All leads are pre-scheduled.</p>
                             <div class="col-md-12  " style="display:flex!important; ">
                          <a href="" class=" b_btn btn btn-secondary text-light rounded-pills ms-3">Apply Now</a>  
                          <a href=""class=" b_btn btnn rounded-pills  ms-3">Save Later</a>
                    </div>
                </div>
                    </div>
                   <div class="col-lg-4 col-md-6 wow zoomIn" data-wow-delay="0.6s">
                         <div class="service-item d-flex flex-column justify-content-center  rounded">
                            <div class=" flex-shrink-0">
                                <img src="img/jobs_logo.png">

                            </div>
                            <p>Uviqo</p>
                            <h5 class="mb-3">Java Developer</h5>
                            <p><span style="margin-left: unset!important;" class="ser">Cochin</span><span class="ser">80k to 90k</span></p>
                            <p>We are looking for Enrolment Advisors who are looking to take 30-35 appointments per week All leads are pre-scheduled.</p>
                             <div class="col-md-12  " style="display:flex!important; ">
                          <a href="" class=" b_btn btn btn-secondary text-light rounded-pills ms-3">Apply Now</a>  
                          <a href=""class=" b_btn btnn rounded-pills  ms-3">Save Later</a>
                    </div>
                </div>
                    </div>
                    <div class="col-lg-4 col-md-6 wow zoomIn" data-wow-delay="0.3s">
                          <div class="service-item d-flex flex-column justify-content-center  rounded">
                            <div class=" flex-shrink-0">
                                <img src="img/jobs_logo.png">

                            </div>
                            <p>Uviqo</p>
                            <h5 class="mb-3">Java Developer</h5>
                            <p><span style="margin-left: unset!important;" class="ser">Cochin</span><span class="ser">80k to 90k</span></p>
                            <p>We are looking for Enrolment Advisors who are looking to take 30-35 appointments per week All leads are pre-scheduled.</p>
                              <div class="col-md-12  " style="display:flex!important; ">
                          <a href="" class=" b_btn btn btn-secondary text-light rounded-pills ms-3">Apply Now</a>  
                          <a href=""class=" b_btn btnn rounded-pills  ms-3">Save Later</a>
                    </div>
                </div>
                    </div>
                    <div class="col-lg-4 col-md-6 wow zoomIn" data-wow-delay="0.6s">
                         <div class="service-item d-flex flex-column justify-content-center  rounded">
                            <div class=" flex-shrink-0">
                                <img src="img/jobs_logo.png">

                            </div>
                            <p>Uviqo</p>
                            <h5 class="mb-3">Java Developer</h5>
                            <p><span style="margin-left: unset!important;" class="ser">Cochin</span><span class="ser">80k to 90k</span></p>
                            <p>We are looking for Enrolment Advisors who are looking to take 30-35 appointments per week All leads are pre-scheduled.</p>
                             <div class="col-md-12  " style="display:flex!important; ">
                          <a href="" class=" b_btn btn btn-secondary text-light rounded-pills ms-3">Apply Now</a>  
                          <a href=""class=" b_btn btnn rounded-pills  ms-3">Save Later</a>
                    </div>
                </div>
                    </div>
                </div>
            </div>
        </div>



        
         <div class="container-xxl py-5">
            <div class="container px-lg-5">
                <div class="row g-5">

                    <div class="col-lg-6 col-md-6">
                        <img class="img-fluid wow zoomIn" data-wow-delay="0.5s" src="img/b_banner.png">
                    </div>

                    <div class="col-lg-6 col-md-6 wow fadeInUp" data-wow-delay="0.1s">
                        <div class="section-title position-relative mb-4 pb-2">
                            
                            <h2 class="mt-2">Get applications from the<br> world best talents.</h2>
                        </div>
                        <p class="mb-4">Capital on hanging fruit to identify a balpark value added activity to<br>Override the digital divide with additional clickthroughs from DevOps.</p>
                        <div class="row g-3">
                            <div class="col-sm-12">
                                <h6 class="mb-3"><i class="fa fa-check text-primary me-2"></i>Bring to the table win-win survival</h6>
                                <h6 class="mb-3"><i class="fa fa-check text-primary me-2"></i>Capitalize on low hanging fruit to identify</h6>
                                <h6 class="mb-3"><i class="fa fa-check text-primary me-2"></i>But I must explain to you how all this</h6>
                            </div>
                        </div>
                        
                    </div>
                    
                </div>
            </div>
        </div>



 <div style="background-color: #F0F8FEFF;" class="container-xxl   wow fadeInUp" data-wow-delay="0.1s">
            <div class="container px-lg-5">
                <div class="row align-items-center f_sec" style="height: 120px;">
                    <div class="mt-2 col-md-6">
                        <h3 >Most comprehensive job portal</h3>
                        <small >We must explain to you how all this mistaken idea of donouncing.</small>
                        <div class="position-relative w-100 mt-3">
                            
                        </div>
                    </div>
                    <div class="col-md-6 text-center d-md-block">
                         <a href=""style="color: #1641d0;
    border: 1px solid #1641d0;
    background-color: white;" class=" rounded-pills py-2 px-4 ms-3">LOGIN</a>
                    <a href="" class="btn btn-secondary text-light rounded-pills py-2 px-4 ms-3">REGISTER</a>
                   
                    </div>
                </div>
            </div>
            <hr style="margin: 0rem 0!important;">
        </div>

        <!-- Footer Start -->
        <div style="background-color: #F0F8FEFF !important;" class="container-fluid   footer  wow fadeIn" data-wow-delay="0.1s">
            <div class="container pt-4 px-lg-5">
                <div class="row g-5">
                    <div class="col-md-6 col-lg-2">
                        <img src="img/footer_logo.png">
                    </div>
                    <div class="col-md-6 col-lg-2 li_new">
                        <h5 class=" mb-4">Product</h5>
                        <a class="btn btn-link" href="">Features</a>
                        <a class="btn btn-link" href="">Pricing</a>
                    </div>
                    <div class="col-md-6 col-lg-2 li_new">
                        <h5 class=" mb-4">Resources</h5>
                        <a class="btn btn-link" href="">Blog</a>
                        <a class="btn btn-link" href="">User guides</a>
                        <a class="btn btn-link" href="">Webinars</a>
                    </div>
                    <div class="col-md-6 col-lg-2 li_new">
                        <h5 class=" mb-4">Community</h5>
                       <a class="btn btn-link" href="">Developer</a>
                        <a class="btn btn-link" href="">Users</a>
                    </div>
                    <div class="col-md-6 col-lg-2 li_new">
                        <h5 class=" mb-4">Company</h5>
                      <a class="btn btn-link" href="">About</a>
                        <a class="btn btn-link" href="">Join us</a>
                        </div>
                        <div class="col-md-6 col-lg-2 li_new">
                        <h5 class=" mb-4">Support</h5>
                      <a class="btn btn-link" href="">Help Center</a>
                        <a class="btn btn-link" href="">Chat Support</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid " style="background-color: #f0f8fe!important;" >
                <div class="copyright">
                    <div class="row">
                        <div class="col-md-10  text-center mt-3 mb-md-0">
                            &copy; <a class="border-bottom" href="#">2022</a>, All Right Reserved. 
                            
                            Designed By <a class="border-bottom" href="https://uviqo.com/">Uviqo</a>
                        </div>
                        <div class="col-md-2 text-md-end">
                            <div class="d-flex pt-2">
                            <a class="btn  btn-social" style="color: skyblue;" href=""><i class="fab fa-twitter"></i></a>
                            <a class="btn  btn-social" style="color: blue;" href=""><i class="fab fa-facebook-f"></i></a>
                            <a class="btn  btn-social" style="color: darkblue;" href=""><i class="fab fa-linkedin-in"></i></a>
                            <a class="btn  btn-social" style="color: red;" href=""><i class="fab fa-youtube"></i></a>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Footer End -->
       

        <!-- Back to Top -->
        <a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top pt-2"><i class="bi bi-arrow-up"></i></a>
    </div>

    <!-- JavaScript Libraries -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="lib/wow/wow.min.js"></script>
    <script src="lib/easing/easing.min.js"></script>
    <script src="lib/waypoints/waypoints.min.js"></script>
    <script src="lib/owlcarousel/owl.carousel.min.js"></script>
    <script src="lib/isotope/isotope.pkgd.min.js"></script>
    <script src="lib/lightbox/js/lightbox.min.js"></script>

    <!-- Template Javascript -->
    <script src="js/main.js"></script>
</body>

</html>