/*
     * this page only using hireq web page..
     * ---------------------------
     * created by : nikhilesh babu mt
     * created Date : Wed , Dec 7
     * */

$(document).ready(function () {
    getOrganisationLogo();
    getJobDetails();
    getCandidateDetails();
});

const contextPath = "http://localhost:8081/"

/* render login page */
function hireqLoginPage() {
    window.open("http://localhost/");
}

/* render sing up page */
function hireqSignUpPage() {
    window.open("http://localhost/signUp");
}

/* using get call only */
function getData(ajaxurl) {
    return $.ajax({
        url: ajaxurl,
        type: 'GET',
    });
};

/* get organisation logo  */
async function getOrganisationLogo() {
    var el ='';
    try {
        const res = await getData(contextPath + 'webPageController/getOrgLogo')
        var array = res.data;
        for (let index = 0; index < 10; index++) {
            const element = array[index];
            var imageName = element.slice(25);
            var ogimage = contextPath + "webPageController/logoProfileDownload/" + imageName;
            el += `
            <div class="product-card">
                            <div class="product-image">
                                <img src="${ogimage}" class="product-thumb">
                            </div>
                        </div>
            `
            $('#logo').html(el);
      
        }
    } catch (err) {
        console.log(err);
    }
}


/* get job details  */
async function getJobDetails() {
    var el ='';
    try {
        const res = await getData(contextPath + 'webPageController/getJobDetails')
        if (res.data != null && res.data.length > 0) {
            $.each(res.data, function (idx, li) {
              el += ` <div class="col-lg-4 col-md-6 wow zoomIn" data-wow-delay="0.1s">
              <div class="service-item d-flex flex-column justify-content-center  rounded">
                  <div class=" flex-shrink-0">
                      <img src="img/jobs_logo.png">

                  </div>
                  <p class="mt-2">${li.orgName}</p>
                  <h5 class="mb-3">${li.title}</h5>
                  <p><span style="margin-left: unset!important;" class="ser">${li.stateName}</span><span
                          class="ser">${li.salary}</span></p>
                  <p>${li.description}</p>
                  <div class="col-md-12  " style="display:flex!important; ">
                      <a href="" class=" b_btn btn btn-secondary text-light rounded-pills ms-3" onclick="hireqLoginPage()">Apply Now</a>
                      <a href="" class=" b_btn btnn rounded-pills  ms-3" onclick="hireqLoginPage()">Save Later</a>
                  </div>
              </div>
          </div>`
          $('#jobList').html(el);
            })
        }
    } catch (err) {
        console.log(err);
    }
}


/* get candidate details  */
async function getCandidateDetails() {
    var el ='';
    try {
        const res = await getData(contextPath + 'webPageController/getCandidateDetails')
        if (res.data != null && res.data.length > 0) {
            console.log("response",res.data)
            $.each(res.data, function (idx, li) {
              el += `  <div class="col-lg-4 col-md-6 wow zoomIn" data-wow-delay="0.1s">
              <div class="service-item d-flex flex-column justify-content-center  rounded" >
                  <div class=" flex-shrink-0">
                      <img src="img/c_img.png">

                  </div>
                  <p class="mt-2">${li.firstName}</p>
                  <h5 class="mb-3">${li.jobRole}</h5>
                  <p>
                  <span style="margin-left: unset!important;" class="ser"> ${li.yearOfExperience} year experience</span>
              
                  </p>
                  <p>${li.profileDescription}</p>
                  <div class="col-md-12  " style="display:flex!important; ">
                      <a href="" class=" b_btn btn btn-secondary text-light rounded-pills ms-3" onclick="hireqLoginPage()">View
                          Profile</a>
                      <a href="" class=" b_btn btnn rounded-pills  ms-3" onclick="hireqLoginPage()">Save Later</a>
                  </div>
              </div>
          </div>`
          $('#candidate-list').html(el);
            })
        }
    } catch (err) {
        console.log(err);
    }
}